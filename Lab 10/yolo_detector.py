from keras.models import load_model
import cv2
import numpy as np
from utils import sigmoid


class YoloDetector:
    """
    Represents an object detector for robot soccer based on the YOLO algorithm.
    """
    def __init__(self, model_name, anchor_box_ball=(5, 5), anchor_box_post=(2, 5)):
        """
        Constructs an object detector for robot soccer based on the YOLO algorithm.

        :param model_name: name of the neural network model which will be loaded.
        :type model_name: str.
        :param anchor_box_ball: dimensions of the anchor box used for the ball.
        :type anchor_box_ball: bidimensional tuple.
        :param anchor_box_post: dimensions of the anchor box used for the goal post.
        :type anchor_box_post: bidimensional tuple.
        """
        self.network = load_model(model_name + '.hdf5')
        self.network.summary()  # prints the neural network summary
        self.anchor_box_ball = anchor_box_ball
        self.anchor_box_post = anchor_box_post

    def detect(self, image):
        """
        Detects robot soccer's objects given the robot's camera image.

        :param image: image from the robot camera in 640x480 resolution and RGB color space.
        :type image: OpenCV's image.
        :return: (ball_detection, post1_detection, post2_detection), where each detection is given
                by a 5-dimensional tuple: (probability, x, y, width, height).
        :rtype: 3-dimensional tuple of 5-dimensional tuples.
        """
        # Todo: implement object detection logic
        output = self.network.predict(self.preprocess_image(image))
        return self.process_yolo_output(output)

    def preprocess_image(self, image):
        """
        Preprocesses the camera image to adapt it to the neural network.

        :param image: image from the robot camera in 640x480 resolution and RGB color space.
        :type image: OpenCV's image.
        :return: image suitable for use in the neural network.
        :rtype: NumPy 4-dimensional array with dimensions (1, 120, 160, 3).
        """
        # implement image preprocessing logic
        image = cv2.resize(image, (160, 120), interpolation=cv2.INTER_AREA)
        image = np.array(image)
        image = image / 255.0
        image = np.reshape(image, (1, 120, 160, 3))
        return image

    def process_yolo_output(self, output):
        """
        Processes the neural network's output to yield the detections.

        :param output: neural network's output.
        :type output: NumPy 4-dimensional array with dimensions (1, 15, 20, 10).
        :return: (ball_detection, post1_detection, post2_detection), where each detection is given
                by a 5-dimensional tuple: (probability, x, y, width, height).
        :rtype: 3-dimensional tuple of 5-dimensional tuples.
        """
        coord_scale = 4 * 8  # coordinate scale used for computing the x and y coordinates of the BB's center
        bb_scale = 640  # bounding box scale used for computing width and height
        output = np.reshape(output, (15, 20, 10))  # reshaping to remove the first dimension

        # implement YOLO logic
        # ball
        pwb, phb = (5, 5)
        balls_p = output[:, :, 0]
        ball_index = np.unravel_index(np.argmax(balls_p, axis=None), balls_p.shape)
        ball_vector = output[ball_index[0], ball_index[1], :]
        ball_detection = (sigmoid(ball_vector[0]),
                          (ball_index[1] + sigmoid(ball_vector[1])) * coord_scale,
                          (ball_index[0] + sigmoid(ball_vector[2])) * coord_scale,
                          bb_scale * pwb * np.exp(ball_vector[3]),
                          bb_scale * phb * np.exp(ball_vector[4]))

        # posts
        pwb, phb = (2, 5)
        posts_p = output[:, :, 5]
        post_1_index, post_2_index = np.array(np.unravel_index(np.argsort(-posts_p, axis=None),
                                                               posts_p.shape)).T[:2]

        # post 1
        post_1_vector = output[post_1_index[0], post_1_index[1], :]
        post1_detection = (sigmoid(post_1_vector[5]),
                          (post_1_index[1] + sigmoid(post_1_vector[6])) * coord_scale,
                          (post_1_index[0] + sigmoid(post_1_vector[7])) * coord_scale,
                          bb_scale * pwb * np.exp(post_1_vector[8]),
                          bb_scale * phb * np.exp(post_1_vector[9]))

        # post 2
        post_2_vector = output[post_2_index[0], post_2_index[1], :]
        post2_detection = (sigmoid(post_2_vector[5]),
                          (post_2_index[1] + sigmoid(post_2_vector[6])) * coord_scale,
                          (post_2_index[0] + sigmoid(post_2_vector[7])) * coord_scale,
                          bb_scale * pwb * np.exp(post_2_vector[8]),
                          bb_scale * phb * np.exp(post_2_vector[9]))
        return ball_detection, post1_detection, post2_detection
