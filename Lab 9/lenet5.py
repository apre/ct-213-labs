from keras import layers, activations
from keras.models import Sequential
from keras import layers


def make_lenet5():
    # implement LeNet-5 model
    model = Sequential()
    model.add(layers.Conv2D(input_shape=(32, 32, 1), filters=6, kernel_size=(5, 5), strides=1, activation='tanh'))
    model.add(layers.AveragePooling2D(pool_size=(2, 2), strides=2))
    model.add(layers.Conv2D(filters=16, kernel_size=(5, 5), strides=1, activation='tanh'))
    model.add(layers.AveragePooling2D(pool_size=(2, 2), strides=2))
    model.add(layers.Conv2D(filters=120, kernel_size=(5, 5), strides=1, activation='tanh'))
    model.add(layers.Flatten())
    model.add(layers.Dense(84, activation='tanh'))
    model.add(layers.Dense(10, activation='softmax'))
    return model


if __name__ == '__main__':
    model = make_lenet5()
    print(model.summary())
