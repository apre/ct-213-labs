from grid import Node, NodeGrid
from math import inf
from heapq import heappop, heappush


class PathPlanner(object):
    """
    Represents a path planner, which may use Dijkstra, Greedy Search or A* to plan a path.
    """
    def __init__(self, cost_map):
        """
        Creates a new path planner for a given cost map.

        :param cost_map: cost used in this path planner.
        :type cost_map: CostMap.
        """
        self.cost_map = cost_map
        self.node_grid = NodeGrid(cost_map)

    @staticmethod
    def construct_path(goal_node):
        """
        Extracts the path after a planning was executed.

        :param goal_node: node of the grid where the goal was found.
        :type goal_node: Node.
        :return: the path as a sequence of (x, y) positions: [(x1,y1),(x2,y2),(x3,y3),...,(xn,yn)].
        :rtype: list of tuples.
        """
        node = goal_node
        # Since we are going from the goal node to the start node following the parents, we
        # are transversing the path in reverse
        reversed_path = []
        while node is not None:
            reversed_path.append(node.get_position())
            node = node.parent
        return reversed_path[::-1]  # This syntax creates the reverse list

    def cost(self, node, successor):
        return self.cost_map.get_edge_cost(node.get_position(),
                                           successor.get_position())

    @staticmethod
    def h(node, goal):
        """
        Calculates heuristic cost to goal from node
        :param node: start node
        :param goal: goal position tuple
        :return: heuristic cost float
        """
        return node.distance_to(*goal)

    def dijkstra(self, start_position, goal_position):
        """
        Plans a path using the Dijkstra algorithm.

        :param start_position: position where the planning stars as a tuple (x, y).
        :type start_position: tuple.
        :param goal_position: goal position of the planning as a tuple (x, y).
        :type goal_position: tuple.
        :return: the path as a sequence of positions and the path cost.
        :rtype: list of tuples and float.
        """
        # The first return is the path as sequence of tuples (as returned by the method construct_path())
        # The second return is the cost of the path
        self.node_grid.reset()

        # initialization
        pq = []
        start_node = self.node_grid.get_node(*start_position)
        start_node.g = 0
        start_node.closed = False
        heappush(pq, (start_node.g, start_node))

        while pq:
            # get next node and mark as explored
            g, node = heappop(pq)
            while node.closed:
                g, node = heappop(pq)
            node.closed = True

            # check if goal was reached
            if node.get_position() == goal_position:
                return self.construct_path(node), node.g

            # add successors to queue
            for successor_position in self.node_grid.get_successors(*node.get_position()):
                successor = self.node_grid.get_node(*successor_position)
                if not successor.closed and successor.g > node.g + self.cost(node, successor):
                    successor.g = node.g + self.cost(node, successor)
                    successor.parent = node
                    successor.closed = False
                    heappush(pq, (successor.g, successor))

    def greedy(self, start_position, goal_position):
        """
        Plans a path using greedy search.

        :param start_position: position where the planning stars as a tuple (x, y).
        :type start_position: tuple.
        :param goal_position: goal position of the planning as a tuple (x, y).
        :type goal_position: tuple.
        :return: the path as a sequence of positions and the path cost.
        :rtype: list of tuples and float.
        """
        # implement the Greedy Search algorithm
        # The first return is the path as sequence of tuples (as returned by the method construct_path())
        # The second return is the cost of the path
        self.node_grid.reset()

        # initialization
        pq = []
        start_node = self.node_grid.get_node(*start_position)
        start_node.g = 0
        # start_node.closed = False
        heappush(pq, (0, start_node))

        while pq:
            # get next node and mark as explored
            h, node = heappop(pq)
            while node.closed and pq:
                h, node = heappop(pq)
            node.closed = True

            # add successors to queue
            successors = [self.node_grid.get_node(*successor_position)
                          for successor_position in self.node_grid.get_successors(*node.get_position())]
            for successor in successors:
                if not successor.closed:
                    successor.g = node.g + self.cost(node, successor)
                    successor.parent = node

                    # check success
                    if successor.get_position() == goal_position:
                        return self.construct_path(successor), successor.g

                    # push successor to priority queue
                    heappush(pq, (successor.distance_to(*goal_position), successor))

    def a_star(self, start_position, goal_position):
        """
        Plans a path using A*.

        :param start_position: position where the planning stars as a tuple (x, y).
        :type start_position: tuple.
        :param goal_position: goal position of the planning as a tuple (x, y).
        :type goal_position: tuple.
        :return: the path as a sequence of positions and the path cost.
        :rtype: list of tuples and float.
        """
        # implement the A* algorithm
        # The first return is the path as sequence of tuples (as returned by the method construct_path())
        # The second return is the cost of the path
        self.node_grid.reset()

        # initialization
        pq = []
        start_node = self.node_grid.get_node(*start_position)
        start_node.g = 0
        start_node.h = self.h(start_node, goal_position)
        heappush(pq, (start_node.g, start_node))

        while pq:
            # get next node and mark as explored
            g, node = heappop(pq)
            while node.closed and pq:
                g, node = heappop(pq)
            node.closed = True

            # check if goal was reached
            if node.get_position() == goal_position:
                return self.construct_path(node), node.f

            # add successors to queue
            for successor_position in self.node_grid.get_successors(*node.get_position()):
                successor = self.node_grid.get_node(*successor_position)
                if successor.f > node.g + self.cost(node, successor) + self.h(successor, goal_position) and not successor.closed:
                    successor.g = node.g + self.cost(node, successor)
                    successor.f = successor.g + self.h(successor, goal_position)
                    successor.parent = node
                    successor.closed = False
                    heappush(pq, (successor.f, successor))
